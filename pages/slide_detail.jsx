import React, { Component } from "react";
import PT from "prop-types";
import { inject, observer } from "mobx-react";
import * as Yup from "yup";
import { withRouter } from "next/router";
import { Divider } from "antd";

import { Layout, CustomAdminContent, CustomAdminUploader } from "../components";
import {
    AdminForm,
    AdminTextInput,
    AdminTextarea,
    AdminTranslateTab,
    AdminDatePicker,
} from "redline-admin";
import { languages } from "../config/langguages";
import routes from "../routes";
import { translateSchemaRule, securePageHoc } from "../utils";

const schema = Yup.object().shape({
    published_at: Yup.date().required("Выбрать дату публикации"),
    ...translateSchemaRule(
        "title",
        Yup.string().required("Введите заголовок 1")
    ),
    ...translateSchemaRule(
        "title2",
        Yup.string().required("Введите заголовок 2")
    ),
    ...translateSchemaRule(
        "sub_title",
        Yup.string().required("Введите саб заголвок")
    ),
    link: Yup.string().nullable(),
    button_text: Yup.string().nullable(),
    ...translateSchemaRule(
        "description",
        Yup.string().required("Введите текст слайда")
    ),
    image: Yup.object().required("Загрузите изображение"),
    background_image: Yup.string().nullable(),
    video: Yup.object(),
    background_color: Yup.string().nullable(),
});

@securePageHoc
@withRouter
@inject("slide_store")
@observer
class SlideDetail extends Component {
    static propTypes = {
        slide_store: PT.object.isRequired,
        router: PT.object.isRequired,
    };

    componentDidMount() {
        const { slide_store, router } = this.props;
        if (router.query.id !== "_") {
            slide_store.getOne(router.query.id);
        } else {
            slide_store.reset();
            slide_store.setReady();
        }
    }

    render() {
        const {
            slide_store: { item },
            slide_store,
        } = this.props;
        return (
            <Layout>
                <CustomAdminContent
                    routes={routes}
                    title={
                        (() => false)()
                            ? "Добавление слайда"
                            : "Редактирование слайда"
                    }
                >
                    <AdminForm
                        item={item}
                        store={slide_store}
                        router={routes.Router}
                        validationSchema={schema}
                    >
                        <AdminDatePicker
                            name="published_at"
                            placeholder="Выбрать дату"
                            label="Дата публикации"
                        />
                        <AdminTranslateTab
                            name="sub_title"
                            languages={languages}
                        >
                            <AdminTextInput label="Саб заголовок" />
                        </AdminTranslateTab>
                        <AdminTranslateTab name="title" languages={languages}>
                            <AdminTextInput label="Заголвок строка 1" />
                        </AdminTranslateTab>
                        <AdminTranslateTab name="title2" languages={languages}>
                            <AdminTextInput label="Заголвок строка 2" />
                        </AdminTranslateTab>
                        <AdminTextInput name="link" label="Ссылка слайда" />
                        <AdminTranslateTab
                            name="button_text"
                            languages={languages}
                        >
                            <AdminTextInput label="Текст кнопки" />
                        </AdminTranslateTab>
                        <AdminTranslateTab
                            name="description"
                            languages={languages}
                        >
                            <AdminTextarea label="Текст слайда" />
                        </AdminTranslateTab>
                        <Divider />
                        <CustomAdminUploader
                            name="video"
                            label="Видео"
                            allowed_types={[
                                "video/mp4",
                                "video/mpeg4",
                                "video/webm",
                            ]}
                        />
                        <CustomAdminUploader name="image" label="Изображение" />
                        <Divider />
                        <CustomAdminUploader
                            name="background_image"
                            label="Фоновое изображение"
                        />
                        <AdminTextInput
                            name="background_color"
                            label="Цвет фона"
                        />
                    </AdminForm>
                </CustomAdminContent>
            </Layout>
        );
    }
}

export default SlideDetail;
