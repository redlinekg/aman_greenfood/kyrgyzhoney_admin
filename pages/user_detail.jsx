import * as Yup from "yup";

import { AdminForm, AdminSaveFormButton, AdminTextInput } from "redline-admin";
import React, { Component } from "react";

import { Layout, CustomAdminContent } from "../components";
import routes from "../routes";

const schema = Yup.object().shape({
    name: Yup.string().required("Введите имя"),
    phone: Yup.string(),
    email: Yup.string()
        .email("Введите валидный e-mail адрес")
        .required("Введите e-mail адрес"),
});

export default class PageUserDetail extends Component {
    render() {
        return (
            <Layout>
                <CustomAdminContent
                    title={
                        (() => false)()
                            ? "Добавление пользователя"
                            : "Редактирование пользователя"
                    }
                    delite
                    routes={routes}
                >
                    <AdminForm router={routes.Router} validationSchema={schema}>
                        <AdminTextInput label="Имя" name="name" />
                        <AdminTextInput
                            label="Телефон"
                            name="phone"
                            type="tel"
                        />
                        <AdminTextInput
                            label="E-mail адрес"
                            name="email"
                            type="email"
                        />
                        <AdminSaveFormButton />
                    </AdminForm>
                </CustomAdminContent>
            </Layout>
        );
    }
}
