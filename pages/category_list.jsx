import { inject, observer } from "mobx-react";
import PT from "prop-types";
import React, { Component } from "react";
import {
    AdminTable,
    AdminTableOperation,
    AdminTableTitle,
} from "redline-admin";
import { Layout, CustomAdminContent } from "../components";
import routes from "../routes";
import { securePageHoc } from "../utils";

const COLUMNS = [
    {
        title: "Заголовок",
        dataIndex: "title",
        key: "title",
        render: (text, record) => (
            <AdminTableTitle
                title={text}
                route="category_detail"
                params={{ id: record.id + "" }}
            />
        ),
        width: "50%",
    },
    {
        title: "Действие",
        key: "actions",
        width: "1%",
        render: (_, record) => {
            return (
                <AdminTableOperation
                    item={record}
                    route="category_detail"
                    params={{ id: record.id + "" }}
                    remove={false}
                />
            );
        },
    },
];

@securePageHoc
@inject("categories_store")
@observer
class CategoryListPage extends Component {
    static propTypes = {
        categories_store: PT.object,
    };
    componentDidMount() {
        this.props.categories_store.getList();
    }
    render() {
        const {
            categories_store: { objects },
        } = this.props;
        return (
            <Layout>
                <CustomAdminContent
                    routes={routes}
                    title="Категории"
                    desk={`${objects.length} категорий`}
                    create_route="category_detail"
                    fit
                    back_button_show={false}
                >
                    <AdminTable
                        rowKey="id"
                        columns={COLUMNS}
                        dataSource={objects}
                    />
                </CustomAdminContent>
            </Layout>
        );
    }
}

export default CategoryListPage;
