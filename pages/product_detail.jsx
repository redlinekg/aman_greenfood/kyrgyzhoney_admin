import axios from "axios";
import { CategoriesStore } from "kyrgyzhoney_mobx";
import { inject, observer } from "mobx-react";
import { onPatch } from "mobx-state-tree";
import { withRouter } from "next/router";
import PT from "prop-types";
import React, { Component } from "react";
import {
    AdminForm,
    AdminFormRepeater,
    AdminSelect,
    AdminTabs,
    AdminTextarea,
    AdminTextInput,
    AdminTitleInput,
    AdminTranslateTab,
    AdminCheckbox,
    AdminDatePicker
} from "redline-admin";
import * as Yup from "yup";
import {
    Layout,
    CustomAdminContent,
    CustomAdminEditorJs as AdminEditorJs,
    CustomAdminMultiUploader,
    CustomAdminUploader,
    MetaTabFields,
} from "../components";
import { languages } from "../config/langguages";
import routes from "../routes";
import {
    getMetaSchema,
    logger,
    translateSchemaRule,
    securePageHoc,
} from "../utils";

const { TabPane } = AdminTabs;

const schema = Yup.object().shape({
    published_at: Yup.date().required("Выбрать дату публикации"),
    ...translateSchemaRule("title", Yup.string().required("Введите заголовок")),
    slug: Yup.string().required("Введите slug"),
    category: Yup.object().required("Выберите категорию"),
    image: Yup.string().required("Загрузите основное изображение"),
    ...translateSchemaRule(
        "description",
        Yup.string().required("Введите описание")
    ),
    ...translateSchemaRule("volume", Yup.string().required("Впишите грамовку")),
    pin: Yup.boolean(),
    header_background: Yup.object().required("Выберете изображение для хедера"),
    ...translateSchemaRule("body", Yup.string().nullable()),
    properties: Yup.array(
        Yup.object({
            ...translateSchemaRule(
                "type",
                Yup.string().required("Обязательно")
            ),
            ...translateSchemaRule(
                "value",
                Yup.string().required("Обязательно")
            ),
        })
    ),
    video: Yup.object().nullable(),
    images: Yup.array().required("Добавьте фотографии"),
    ...getMetaSchema(),
});

@securePageHoc
@withRouter
@inject("product_store")
@observer
class ProdutDetailPage extends Component {
    static propTypes = {
        product_store: PT.object.isRequired,
        router: PT.object.isRequired,
    };
    categories_store = CategoriesStore.create(
        {},
        {
            axios,
            logger,
        }
    );

    componentDidMount() {
        this.categories_store.getList();
        onPatch(this.categories_store, () => this.forceUpdate);
        //
        const { product_store, router } = this.props;
        if (router.query.id !== "_") {
            product_store.getOne(router.query.id);
        } else {
            product_store.reset();
            product_store.setReady();
        }
    }

    render() {
        const {
            product_store: { item },
            product_store,
        } = this.props;
        this.categories_store.is_loading;
        return (
            <Layout>
                <CustomAdminContent
                    routes={routes}
                    title={
                        (() => false)()
                            ? "Добавление товара"
                            : "Редактирование товара"
                    }
                >
                    <AdminForm
                        item={item}
                        store={product_store}
                        router={routes.Router}
                        validationSchema={schema}
                    >
                        <AdminTabs defaultActiveKey="1">
                            <TabPane tab="Основное" key="1">
                                <AdminDatePicker
                                    name="published_at"
                                    placeholder="Выбрать дату"
                                    label="Дата публикации"
                                />
                                <AdminTranslateTab
                                    name="title"
                                    languages={languages}
                                >
                                    <AdminTitleInput
                                        autoslug={!item || !item.id}
                                        label="Заголовок"
                                        slug_name="slug"
                                        slug_label="Слаг"
                                    />
                                </AdminTranslateTab>
                                <AdminSelect
                                    label="Категория"
                                    placeholder="Выберите категорию"
                                    name="category"
                                    variants={this.categories_store.objects.map(
                                        (category) => ({
                                            value: category,
                                            label: category.title,
                                        })
                                    )}
                                    prepareValue={(v) => (v ? v.id : null)}
                                    prepareVariantValue={(v) => v.id}
                                    convertValue={(id) =>
                                        this.categories_store.objects.find(
                                            (category) => category.id === id
                                        )
                                    }
                                    loading={this.categories_store.is_loading}
                                />
                                <CustomAdminUploader
                                    name="image"
                                    label="Основное изображение"
                                />
                                <AdminTranslateTab
                                    name="volume"
                                    languages={languages}
                                >
                                    <AdminTextInput label="Грамовка" />
                                </AdminTranslateTab>
                                <AdminTranslateTab
                                    name="description"
                                    languages={languages}
                                >
                                    <AdminTextarea label="Описание" />
                                </AdminTranslateTab>
                                <CustomAdminUploader
                                    name="header_background"
                                    label="Изображение в шапке"
                                />
                                <AdminCheckbox
                                    name="pin"
                                    label="Закрепить в на главной"
                                />
                                <AdminTranslateTab
                                    name="body"
                                    languages={languages}
                                >
                                    <AdminEditorJs label="Тело страницы" />
                                </AdminTranslateTab>
                            </TabPane>
                            {/* ............................................................ */}
                            {/* ............................................................ */}
                            {/* ............................................................ */}
                            <TabPane tab="Состав" key="2">
                                <AdminFormRepeater
                                    name="properties"
                                    label_item="Состав"
                                    button_add_text="Добавить ещё поле"
                                    button_remove_text="Удалить поле"
                                >
                                    <AdminTranslateTab
                                        name="type"
                                        languages={languages}
                                    >
                                        <AdminTextInput label="Свойство" />
                                    </AdminTranslateTab>
                                    <AdminTranslateTab
                                        name="value"
                                        languages={languages}
                                    >
                                        <AdminTextInput label="Значение" />
                                    </AdminTranslateTab>
                                </AdminFormRepeater>
                            </TabPane>
                            {/* ............................................................ */}
                            {/* ............................................................ */}
                            {/* ............................................................ */}
                            <TabPane tab="Медиа, видео и фото" key="3">
                                <CustomAdminUploader
                                    name="video"
                                    label="Видео"
                                    allowed_types={[
                                        "video/mp4",
                                        "video/mpeg4",
                                        "video/webm",
                                    ]}
                                />
                                <CustomAdminMultiUploader
                                    name="images"
                                    label="Фотографии"
                                />
                            </TabPane>
                            {/* ............................................................ */}
                            {/* ............................................................ */}
                            {/* ............................................................ */}
                            <TabPane tab="SEO" key="4">
                                <MetaTabFields />
                            </TabPane>
                        </AdminTabs>
                    </AdminForm>
                </CustomAdminContent>
            </Layout>
        );
    }
}

export default ProdutDetailPage;
