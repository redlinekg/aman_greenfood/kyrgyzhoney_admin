import React, { Component } from "react";
import PT from "prop-types";
import * as Yup from "yup";
import { inject, observer } from "mobx-react";
import { withRouter } from "next/router";

import { Layout, CustomAdminContent } from "../components";
import {
    AdminForm,
    AdminSelect,
    AdminTextInput,
    AdminTranslateTab,
    AdminFormRepeater,
    AdminCheckbox,
    AdminDatePicker,
} from "redline-admin";
import { languages } from "../config/langguages";
import routes from "../routes";
import { translateSchemaRule, securePageHoc } from "../utils";

const schema = Yup.object().shape({
    published_at: Yup.date().required("Выбрать дату публикации"),
    ...translateSchemaRule("title", Yup.string().required("Введите заголовок")),
    //
    global: Yup.boolean(),
    //
    items: Yup.array().of(
        Yup.object().shape({
            type: Yup.string().nullable(),
            ...translateSchemaRule("text", Yup.string().nullable()),
        })
    ),
});

@securePageHoc
@withRouter
@inject("contact_store")
@observer
class ContactDetailPage extends Component {
    static propTypes = {
        contact_store: PT.object.isRequired,
        router: PT.object.isRequired,
    };

    componentDidMount() {
        const { contact_store, router } = this.props;
        if (router.query.id !== "_") {
            contact_store.getOne(router.query.id);
        } else {
            contact_store.reset();
            contact_store.setReady();
        }
    }

    render() {
        const {
            contact_store: { item },
            contact_store,
        } = this.props;
        return (
            <Layout>
                <CustomAdminContent
                    routes={routes}
                    title={
                        (() => false)()
                            ? "Добавление контакта"
                            : "Редактирование контакта"
                    }
                >
                    <AdminForm
                        item={item}
                        store={contact_store}
                        router={routes.Router}
                        validationSchema={schema}
                    >
                        <AdminDatePicker
                            name="published_at"
                            placeholder="Выбрать дату"
                            label="Дата публикации"
                        />
                        <AdminTranslateTab name="title" languages={languages}>
                            <AdminTextInput label="Заголовок" />
                        </AdminTranslateTab>
                        <AdminCheckbox name="global" label="Основной" />
                        <AdminFormRepeater
                            name="items"
                            label_item="Контакт"
                            button_add_text="Добавить ещё контакт"
                            button_remove_text="Удалить уонтакт"
                        >
                            <AdminSelect
                                label="Тип контакта"
                                placeholder="Выберите тип"
                                name="type"
                                variants={[
                                    { value: "text", label: "Текст" },
                                    { value: "link", label: "Ссылка" },
                                    { value: "phone", label: "Телефон" },
                                    { value: "email", label: "Email" },
                                ]}
                            />
                            <AdminTranslateTab
                                name="text"
                                languages={languages}
                            >
                                <AdminTextInput label="Текст" />
                            </AdminTranslateTab>
                        </AdminFormRepeater>
                    </AdminForm>
                </CustomAdminContent>
            </Layout>
        );
    }
}

export default ContactDetailPage;
