import React, { Component } from "react";
import PT from "prop-types";
import * as Yup from "yup";
import { AdminForm, AdminTextInput, AdminTranslateTab } from "redline-admin";
import { inject, observer } from "mobx-react";
import { withRouter } from "next/router";

import { Layout, CustomAdminContent, CustomAdminUploader } from "../components";
import { languages } from "../config/langguages";
import routes from "../routes";
import { translateSchemaRule, securePageHoc } from "../utils";

const schema = Yup.object().shape({
    ...translateSchemaRule("title", Yup.string().required("Введите заголовок")),
    avatar: Yup.string().required("Загрузите аватар"),
});

@securePageHoc
@withRouter
@inject("article_author_store")
@observer
class PageArticleAuthorDetail extends Component {
    static propTypes = {
        article_author_store: PT.object.isRequired,
        router: PT.object.isRequired,
    };

    componentDidMount() {
        const { article_author_store, router } = this.props;
        if (router.query.id !== "_") {
            article_author_store.getOne(router.query.id);
        } else {
            article_author_store.reset();
            article_author_store.setReady();
        }
    }

    render() {
        const {
            article_author_store: { item },
            article_author_store,
        } = this.props;
        return (
            <Layout>
                <CustomAdminContent
                    routes={routes}
                    title={
                        (() => false)()
                            ? "Добавление автора блога"
                            : "Редактирование автора блога"
                    }
                >
                    <AdminForm
                        item={item}
                        store={article_author_store}
                        router={routes.Router}
                        validationSchema={schema}
                    >
                        <AdminTranslateTab name="title" languages={languages}>
                            <AdminTextInput label="Имя" />
                        </AdminTranslateTab>
                        <CustomAdminUploader name="avatar" label="Аватар" />
                    </AdminForm>
                </CustomAdminContent>
            </Layout>
        );
    }
}

export default PageArticleAuthorDetail;
