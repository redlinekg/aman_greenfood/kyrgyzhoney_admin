import { inject, observer } from "mobx-react";
import PT from "prop-types";
import React, { Component } from "react";
import {
    AdminTable,
    AdminTableImage,
    AdminTableOperation,
    AdminTableTitle,
} from "redline-admin";
import { Layout, CustomAdminContent } from "../components";
import routes from "../routes";
import { securePageHoc } from "../utils";

const COLUMNS = [
    {
        title: "Аватар",
        dataIndex: "avatar",
        key: "avatar",
        render: (avatar) => (
            <AdminTableImage src={avatar ? avatar.url_path : null} />
        ),
        width: "1%",
    },
    {
        title: "Имя",
        dataIndex: "title",
        key: "title",
        render: (text, item) => (
            <AdminTableTitle
                title={text}
                route="article_author_detail"
                params={{ id: item.id + "" }}
            />
        ),
        width: "50%",
    },
    {
        title: "Действие",
        dataIndex: "actions",
        key: "actions",
        width: "1%",
        render: (_, item) => (
            <AdminTableOperation
                item={item}
                route="article_author_detail"
                params={{ id: item.id + "" }}
                remove={false}
            />
        ),
    },
];

@securePageHoc
@inject("article_authors_store")
@observer
class PageArticleAuthorList extends Component {
    static propTypes = {
        article_authors_store: PT.object,
    };

    componentDidMount() {
        this.props.article_authors_store.getList();
    }

    render() {
        const {
            article_authors_store: { objects },
        } = this.props;
        return (
            <Layout>
                <CustomAdminContent
                    routes={routes}
                    title="Авторы"
                    desk={`${objects.length} автора`}
                    create_route="article_author_detail"
                    fit
                    back_button_show={false}
                >
                    <AdminTable columns={COLUMNS} dataSource={objects} />
                </CustomAdminContent>
            </Layout>
        );
    }
}

export default PageArticleAuthorList;
