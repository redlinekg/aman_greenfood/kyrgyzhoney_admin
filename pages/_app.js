import React from "react";
import App from "next/app";
import { Provider } from "mobx-react";
import axios from "axios";
import { createStores, getStore } from "kyrgyzhoney_mobx";
import { isNode } from "browser-or-node";
import { logger } from "../utils";
import { IconsSymbol } from "redline-admin";

import "redline-admin/dist/redline-admin.css";

let stores = null;

class MyApp extends App {
    static async getInitialProps({ Component, ctx, ctx: { req } }) {
        let pageProps = {};
        const promises = [];
        // create new store on each request (on nodejs)
        if (isNode) {
            stores = createStores({
                snapshots: {},
                cookie: req.headers.cookie,
                logger,
                axios,
                baseURL: process.env.API_URL,
            });
        }
        const auth_store = getStore("auth_store");
        if (Component.getInitialProps) {
            promises.push(Component.getInitialProps(ctx));
        } else {
            promises.push(Promise.resolve());
        }
        promises.push(auth_store.getMe());
        [pageProps] = await Promise.all(promises);
        return { pageProps, stores: stores.toJS() };
    }

    constructor(props) {
        super();
        if (!stores) {
            stores = createStores({
                snapshots: props.stores,
                logger,
                axios,
                baseURL: "/",
            });
        }
    }

    render() {
        const { Component, pageProps } = this.props;
        return (
            <Provider {...stores.getStores()}>
                <Component
                    isAuthenticated={stores
                        .getStore("auth_store")
                        .isAuthenticated(true)}
                    {...pageProps}
                />
                <IconsSymbol />
            </Provider>
        );
    }
}

export default MyApp;
