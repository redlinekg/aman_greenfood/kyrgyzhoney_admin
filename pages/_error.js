import React, { PureComponent } from "react";
import { Meta } from "../components";

class Error extends PureComponent {
    render() {
        return (
            <>
                <Meta title="Ошибка" />
                Ошибка
            </>
        );
    }
}

export default Error;
