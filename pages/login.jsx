import React, { Component } from "react";
import PT from "prop-types";
import * as Yup from "yup";
import { styled } from "linaria/react";
import { inject, observer } from "mobx-react";
import {
    AdminSaveFormButton,
    AdminTextInput,
    Formik,
    antd,
} from "redline-admin";

import routes from "../routes";
import { Meta, DangerCactusIcon } from "../components";
import { colors } from "../config/var";

const { Icon } = antd;

const SCHEMA = Yup.object().shape({
    email: Yup.string()
        .email("Введите корректный e-mail адрес")
        .required("Введите вашу почту"),
    password: Yup.string().required("Введите пароль"),
});

@inject("auth_store")
@observer
class LoginPage extends Component {
    static propTypes = {
        auth_store: PT.object,
    };

    render() {
        const { auth_store } = this.props;
        return (
            <SLogin>
                <Meta title="Авторизация" />
                <SBlock>
                    <Formik
                        initialValues={{ email: "", password: "" }}
                        validationSchema={SCHEMA}
                        onSubmit={async (values, { setSubmitting }) => {
                            setSubmitting(true);
                            try {
                                await auth_store.login(values);
                                routes.Router.replace("/");
                            } catch (error) {
                                console.error(error);
                            } finally {
                                setSubmitting(false);
                            }
                        }}
                    >
                        <SForm>
                            <SLogo>
                                <DangerCactusIcon />
                            </SLogo>
                            <STitle>Авторизация</STitle>
                            <SDescription>
                                Введите ваш e-mail адрес и пароль
                            </SDescription>
                            <AdminTextInput
                                name="email"
                                type="email"
                                placeholder="Email адрес"
                                prefix={
                                    <Icon
                                        type="user"
                                        style={{ color: "rgba(0,0,0,.25)" }}
                                    />
                                }
                            />
                            <AdminTextInput
                                name="password"
                                prefix={
                                    <Icon
                                        type="lock"
                                        style={{ color: "rgba(0,0,0,.25)" }}
                                    />
                                }
                                type="password"
                                placeholder="Пароль"
                            />
                            <SSubmit>
                                <AdminSaveFormButton type="danger">
                                    Войти
                                </AdminSaveFormButton>
                            </SSubmit>
                        </SForm>
                    </Formik>
                </SBlock>
            </SLogin>
        );
    }
}

const SLogin = styled.div`
    width: 100%;
    min-height: 100vh;
    display: flex;
    align-items: center;
    justify-content: center;
    background-image: url("/static/img/login_bg_black.svg");
    background-size: cover;
    padding: 2em 15px;
`;
const SBlock = styled.div`
    width: 320px;
`;
const SForm = styled.div`
    width: 100%;
    .ant-row {
        margin-bottom: 1em;
    }
    input {
        height: 40px;
    }
`;
const SLogo = styled.div`
    svg {
        width: 6em;
        display: block;
        margin: 0 auto 1.5em;
    }
`;
const STitle = styled.h1`
    /* color: ${colors.black}; */
    color: ${colors.white};
    font-weight: 600;
    margin-bottom: 0;
    text-align: center;
    font-size: 1.4em;
`;
const SDescription = styled.div`
    color: ${colors.grey_light};
    margin-bottom: 3em;
    text-align: center;
    font-weight: 400;
`;
const SSubmit = styled.div`
    button {
        font-size: 1.1em;
        width: 100%;
        margin: 1em 0 0;
        height: 40px;
    }
`;

export default LoginPage;
