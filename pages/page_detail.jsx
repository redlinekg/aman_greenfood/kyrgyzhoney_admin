import React, { Component } from "react";
import PT from "prop-types";
import { inject, observer } from "mobx-react";
import routes from "../routes";
import * as Yup from "yup";
import { withRouter } from "next/router";

import {
    Layout,
    CustomAdminEditorJs as AdminEditorJs,
    CustomAdminContent,
    MetaTabFields,
    CustomAdminUploader,
} from "../components";
import {
    AdminForm,
    AdminTabs,
    AdminCheckbox,
    AdminTitleInput,
    AdminTextInput,
    AdminTranslateTab,
    AdminDatePicker,
} from "redline-admin";
import { languages } from "../config/langguages";
const { TabPane } = AdminTabs;
import { getMetaSchema, translateSchemaRule, securePageHoc } from "../utils";

const schema = Yup.object().shape({
    published_at: Yup.date().required("Выбрать дату публикации"),
    ...translateSchemaRule("title", Yup.string().required("Введите заголовок")),
    slug: Yup.string().required("Введите slug"),
    ...translateSchemaRule(
        "alt_title",
        Yup.string().required("Введите альтернативный заголовок")
    ),
    is_about_company: Yup.boolean(),
    header_background: Yup.string().nullable(),
    ...translateSchemaRule(
        "body",
        Yup.string().required("Введите текст страницы")
    ),
    ...getMetaSchema(),
});

@securePageHoc
@withRouter
@inject("page_store")
@observer
class PageDetail extends Component {
    static propTypes = {
        page_store: PT.object.isRequired,
        router: PT.object.isRequired,
    };

    componentDidMount() {
        const { page_store, router } = this.props;
        if (router.query.id !== "_") {
            page_store.getOne(router.query.id);
        } else {
            page_store.reset();
            page_store.setReady();
        }
    }

    render() {
        const {
            page_store: { item },
            page_store,
        } = this.props;
        return (
            <Layout>
                <CustomAdminContent
                    routes={routes}
                    title={
                        (() => false)()
                            ? "Добавление страницы"
                            : "Редактирование страницы"
                    }
                >
                    <AdminForm
                        item={item}
                        store={page_store}
                        router={routes.Router}
                        validationSchema={schema}
                    >
                        <AdminTabs defaultActiveKey="1">
                            <TabPane tab="Основное" key="1">
                                <AdminDatePicker
                                    name="published_at"
                                    placeholder="Выбрать дату"
                                    label="Дата публикации"
                                />
                                <AdminTranslateTab
                                    name="title"
                                    languages={languages}
                                >
                                    <AdminTitleInput
                                        autoslug={!item || !item.id}
                                        label="Заголовок"
                                        slug_name="slug"
                                        slug_label="Слаг"
                                    />
                                </AdminTranslateTab>
                                <AdminTranslateTab
                                    name="alt_title"
                                    languages={languages}
                                >
                                    <AdminTextInput label="Альтернативный заголовок" />
                                </AdminTranslateTab>
                                <AdminCheckbox
                                    name="is_about_company"
                                    label="Страница о компании"
                                />
                                <CustomAdminUploader
                                    name="header_background"
                                    label="Изображение для шапки"
                                />
                                <AdminTranslateTab
                                    name="body"
                                    languages={languages}
                                >
                                    <AdminEditorJs label="Тело страницы" />
                                </AdminTranslateTab>
                            </TabPane>
                            <TabPane tab="SEO" key="2">
                                <MetaTabFields />
                            </TabPane>
                        </AdminTabs>
                    </AdminForm>
                </CustomAdminContent>
            </Layout>
        );
    }
}

export default PageDetail;
