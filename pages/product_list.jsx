import { inject, observer } from "mobx-react";
import PT from "prop-types";
import React, { Component } from "react";
import {
    AdminTable,
    AdminTableImage,
    AdminTableOperation,
    AdminTableTitle,
} from "redline-admin";
import { Layout, CustomAdminContent } from "../components";
import routes from "../routes";
import { securePageHoc } from "../utils";

const COLUMNS = [
    {
        title: "Изображение",
        dataIndex: "image",
        key: "image",
        render: (image) => (
            <AdminTableImage src={image ? image.url_path : ""} />
        ),
        width: "1%",
    },
    {
        title: "Заголовок",
        dataIndex: "title",
        key: "title",
        render: (text, record) => (
            <AdminTableTitle
                title={text}
                route="product_detail"
                params={{ id: record.id + "" }}
            />
        ),
        width: "40%",
    },
    {
        title: "Категория",
        dataIndex: "category",
        key: "category",
        render: (category) => category.title,
        width: "10%",
    },
    {
        title: "Закрепленный продукт",
        dataIndex: "pin",
        key: "pin",
        width: "10%",
        render: (pin) => (pin ? "Да" : "Нет"),
    },
    {
        title: "Действие",
        key: "actions",
        width: "1%",
        render: (_, record) => {
            return (
                <AdminTableOperation
                    item={record}
                    route="product_detail"
                    params={{ id: record.id + "" }}
                    remove={false}
                />
            );
        },
    },
];

@securePageHoc
@inject("products_store")
@observer
class ProductListPage extends Component {
    static propTypes = {
        products_store: PT.object,
    };
    componentDidMount() {
        this.props.products_store.getList();
    }
    render() {
        const {
            products_store: { objects },
        } = this.props;
        return (
            <Layout>
                <CustomAdminContent
                    routes={routes}
                    title="Товары"
                    desk={`${objects.length} записей`}
                    create_route="product_detail"
                    fit
                    back_button_show={false}
                >
                    <AdminTable
                        rowKey="id"
                        columns={COLUMNS}
                        dataSource={objects}
                    />
                </CustomAdminContent>
            </Layout>
        );
    }
}

export default ProductListPage;
