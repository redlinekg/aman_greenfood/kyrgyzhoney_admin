import React, { Component } from "react";
import {
    AdminTable,
    AdminTableOperation,
    AdminTableTitle,
} from "redline-admin";
import { Layout, CustomAdminContent } from "../components";

const data = [
    {
        id: "1",
        key: "1",
        email: "vladimir@redline.kg",
        type: "Офис продаж",
    },
];

export default class PageUserList extends Component {
    render() {
        const columns = [
            {
                title: "Id",
                dataIndex: "id",
                key: "type",
                width: "1%",
            },
            {
                title: "Email",
                dataIndex: "email",
                key: "type",
                width: "8%",
                render: (text) => (
                    <AdminTableTitle
                        title={text}
                        route="user_detail"
                        params={{ id: "id" }}
                    />
                ),
            },
            {
                title: "Действие",
                dataIndex: "actions",
                key: "actions",
                width: "1%",
                render: () => (
                    <AdminTableOperation
                        route="user_detail"
                        params={{ id: "1" }}
                        remove={false}
                    />
                ),
            },
        ];
        return (
            <Layout>
                <CustomAdminContent
                    title="Пользователи"
                    desk="1 юзер"
                    fit
                    add
                    back_button_show={false}
                >
                    <AdminTable columns={columns} dataSource={data} />
                </CustomAdminContent>
            </Layout>
        );
    }
}
