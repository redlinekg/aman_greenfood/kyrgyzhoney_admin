import React, { Component } from "react";

import { Layout, CustomAdminContent } from "../components";
import { securePageHoc } from "../utils";
import routes from "../routes";

@securePageHoc
class IndexPage extends Component {
    componentDidMount() {
        routes.Router.pushRoute("/product_list");
    }

    render() {
        return (
            <Layout>
                <CustomAdminContent
                    title="Главная"
                    fit
                    hide_bg
                    back_button_show={false}
                >
                    Главная
                </CustomAdminContent>
            </Layout>
        );
    }
}

export default IndexPage;
