import React, { Component } from "react";
import PT from "prop-types";
import axios from "axios";
import { onPatch } from "mobx-state-tree";
import * as Yup from "yup";
import { inject, observer } from "mobx-react";
import { ArticleAuthorsStore } from "kyrgyzhoney_mobx";
import { withRouter } from "next/router";

import {
    Layout,
    CustomAdminEditorJs as AdminEditorJs,
    CustomAdminContent,
    CustomAdminUploader,
    MetaTabFields,
} from "../components";
import {
    AdminForm,
    AdminSelect,
    AdminTabs,
    AdminCheckbox,
    AdminTextInput,
    AdminTitleInput,
    AdminTranslateTab,
    AdminDatePicker,
} from "redline-admin";
import {
    getMetaSchema,
    logger,
    translateSchemaRule,
    securePageHoc,
} from "../utils";

import routes from "../routes";
import { languages } from "../config/langguages";

const { TabPane } = AdminTabs;

const schema = Yup.object().shape({
    published_at: Yup.date().required("Выбрать дату публикации"),
    ...translateSchemaRule("title", Yup.string().required("Введите заголовок")),
    slug: Yup.string().required("Введите slug"),
    //
    ...translateSchemaRule(
        "description",
        Yup.string().required("Введите описание")
    ),
    //
    author: Yup.string().required("Выберите автора"),
    //
    header_background: Yup.string().nullable(),
    //
    pin: Yup.boolean(),
    image: Yup.string().required("Загрузите изображение"),
    //
    ...translateSchemaRule(
        "body",
        Yup.string().required("Введите текст записи")
    ),
    //
    ...getMetaSchema(),
});

@securePageHoc
@withRouter
@inject("article_store")
@observer
class PageArticleDetail extends Component {
    static propTypes = {
        article_store: PT.object.isRequired,
        router: PT.object.isRequired,
    };

    authors_store = ArticleAuthorsStore.create(
        {},
        {
            axios,
            logger,
        }
    );

    componentDidMount() {
        this.authors_store.getList();
        onPatch(this.authors_store, () => this.forceUpdate());
        //
        const { article_store, router } = this.props;
        if (router.query.id !== "_") {
            article_store.getOne(router.query.id);
        } else {
            article_store.reset();
            article_store.setReady();
        }
    }

    render() {
        const {
            article_store: { item },
            article_store,
        } = this.props;
        this.authors_store.is_loading;
        return (
            <Layout>
                <CustomAdminContent
                    routes={routes}
                    title={
                        (() => false)()
                            ? "Добавление записи"
                            : "Редактирование записи"
                    }
                >
                    <AdminForm
                        item={item}
                        store={article_store}
                        router={routes.Router}
                        validationSchema={schema}
                    >
                        <AdminTabs defaultActiveKey="1">
                            <TabPane tab="Основное" key="1">
                                <AdminDatePicker
                                    name="published_at"
                                    placeholder="Выбрать дату"
                                    label="Дата публикации"
                                />
                                <AdminTranslateTab
                                    name="title"
                                    languages={languages}
                                >
                                    <AdminTitleInput
                                        autoslug={!item || !item.id}
                                        label="Заголовок"
                                        slug_name="slug"
                                        slug_label="Слаг"
                                    />
                                </AdminTranslateTab>
                                <AdminTranslateTab
                                    name="description"
                                    languages={languages}
                                >
                                    <AdminTextInput label="Описание" />
                                </AdminTranslateTab>
                                <AdminSelect
                                    label="Автор"
                                    placeholder="Выберите автора"
                                    name="author"
                                    variants={this.authors_store.objects.map(
                                        (author) => ({
                                            value: author,
                                            label: author.title,
                                        })
                                    )}
                                    prepareValue={(v) => (v ? v.id : null)}
                                    prepareVariantValue={(v) => v.id}
                                    convertValue={(id) =>
                                        this.authors_store.objects.find(
                                            (author) => author.id === id
                                        )
                                    }
                                    loading={this.authors_store.is_loading}
                                />
                                <CustomAdminUploader
                                    name="header_background"
                                    label="Изображение для шапки"
                                />
                                <CustomAdminUploader
                                    name="image"
                                    label="Превью изображение"
                                />
                                <AdminCheckbox
                                    name="pin"
                                    label="Закрепить в каталоге"
                                />
                                <AdminTranslateTab
                                    name="body"
                                    languages={languages}
                                >
                                    <AdminEditorJs label="Тело страницы" />
                                </AdminTranslateTab>
                            </TabPane>
                            <TabPane tab="SEO" key="2">
                                <MetaTabFields />
                            </TabPane>
                        </AdminTabs>
                    </AdminForm>
                </CustomAdminContent>
            </Layout>
        );
    }
}

export default PageArticleDetail;
