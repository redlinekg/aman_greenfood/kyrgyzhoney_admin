import { inject, observer } from "mobx-react";
import PT from "prop-types";
import React, { Component } from "react";
import {
    AdminTable,
    AdminTableDate,
    AdminTableImage,
    AdminTableOperation,
    AdminTableTitle,
} from "redline-admin";
import { Layout, CustomAdminContent } from "../components";
import routes from "../routes";
import { securePageHoc } from "../utils";

const COLUMNS = [
    {
        title: "Изображение",
        dataIndex: "image",
        key: "image",
        render: (image) => <AdminTableImage src={image.url_path} />,
        width: "1%",
    },
    {
        title: "Дата",
        dataIndex: "created_at",
        key: "created_at",
        render: (date) => <AdminTableDate date={date} />,
        width: "10%",
    },
    {
        title: "Заголовок",
        dataIndex: "title",
        key: "title",
        render: (text, record) => (
            <AdminTableTitle
                title={text}
                route="article_detail"
                params={{ id: record.id + "" }}
            />
        ),
        width: "50%",
    },
    {
        title: "Действие",
        key: "actions",
        width: "1%",
        render: (_, record) => {
            return (
                <AdminTableOperation
                    item={record}
                    route="article_detail"
                    params={{ id: record.id + "" }}
                    remove={false}
                />
            );
        },
    },
];

@securePageHoc
@inject("articles_store")
@observer
class ArticleListPage extends Component {
    static propTypes = {
        articles_store: PT.object,
    };
    componentDidMount() {
        this.props.articles_store.getList();
    }
    render() {
        const {
            articles_store: { objects },
        } = this.props;
        return (
            <Layout>
                <CustomAdminContent
                    routes={routes}
                    title="Блог"
                    desk={`${objects.length} записей`}
                    create_route="article_detail"
                    fit
                    back_button_show={false}
                >
                    <AdminTable
                        rowKey="id"
                        columns={COLUMNS}
                        dataSource={objects}
                    />
                </CustomAdminContent>
            </Layout>
        );
    }
}

export default ArticleListPage;
