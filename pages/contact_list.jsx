import { inject, observer } from "mobx-react";
import PT from "prop-types";
import React, { Component } from "react";
import {
    AdminTable,
    AdminTableOperation,
    AdminTableTitle,
} from "redline-admin";
import { Layout, CustomAdminContent } from "../components";
import routes from "../routes";
import { securePageHoc } from "../utils";

const COLUMNS = [
    {
        title: "Заголовок",
        dataIndex: "title",
        key: "title",
        render: (text, record) => (
            <AdminTableTitle
                title={text}
                route="contact_detail"
                params={{ id: record.id + "" }}
            />
        ),
        width: "50%",
    },
    {
        title: "Действие",
        key: "actions",
        width: "1%",
        render: (_, record) => {
            return (
                <AdminTableOperation
                    item={record}
                    route="contact_detail"
                    params={{ id: record.id + "" }}
                    remove={false}
                />
            );
        },
    },
];

@securePageHoc
@inject("contacts_store")
@observer
class ContactListPage extends Component {
    static propTypes = {
        contacts_store: PT.object,
    };
    componentDidMount() {
        this.props.contacts_store.getList();
    }
    render() {
        const {
            contacts_store: { objects },
        } = this.props;
        return (
            <Layout>
                <CustomAdminContent
                    routes={routes}
                    title="Контакты"
                    desk={`${objects.length} контактов`}
                    create_route="contact_detail"
                    fit
                    back_button_show={false}
                >
                    <AdminTable
                        rowKey="id"
                        columns={COLUMNS}
                        dataSource={objects}
                    />
                </CustomAdminContent>
            </Layout>
        );
    }
}

export default ContactListPage;
