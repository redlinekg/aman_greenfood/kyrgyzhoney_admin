import React from "react";
import Document, { Head, Main, NextScript } from "next/document";
import Helmet from "react-helmet";

import apple_touch_icon from "../static/img/fv/apple-touch-icon.png";
import favicon_32x32 from "../static/img/fv/favicon-32x32.png";
import favicon_16x16 from "../static/img/fv/favicon-16x16.png";
import safari_pinned_tab from "../static/img/fv/safari-pinned-tab.svg";

export default class MyDocument extends Document {
    static async getInitialProps(...args) {
        const documentProps = await super.getInitialProps(...args);
        return { ...documentProps, helmet: Helmet.renderStatic() };
    }

    get helmetHtmlAttrComponents() {
        return this.props.helmet.htmlAttributes.toComponent();
    }

    get helmetBodyAttrComponents() {
        return this.props.helmet.bodyAttributes.toComponent();
    }

    get helmetHeadComponents() {
        return Object.keys(this.props.helmet)
            .filter((el) => el !== "htmlAttributes" && el !== "bodyAttributes")
            .map((el) => this.props.helmet[el].toComponent());
    }

    render() {
        return (
            <html>
                <Head>
                    <meta
                        name="viewport"
                        content="width=device-width, minimum-scale=1, maximum-scale=1, initial-scale=1, user-scalable=no"
                    />
                    <meta
                        httpEquiv="content-type"
                        content="text/html; charset=utf-8"
                    />
                    <link
                        rel="apple-touch-icon"
                        sizes="180x180"
                        href={apple_touch_icon}
                    />
                    <link
                        rel="icon"
                        type="image/png"
                        sizes="32x32"
                        href={favicon_32x32}
                    />
                    <link
                        rel="icon"
                        type="image/png"
                        sizes="16x16"
                        href={favicon_16x16}
                    />
                    <link rel="manifest" href="/static/img/fv/manifest.json" />
                    <link
                        rel="mask-icon"
                        href={safari_pinned_tab}
                        color="#ed253f"
                    />
                    <meta
                        name="apple-mobile-web-app-title"
                        content="Danger Admin"
                    />
                    <meta name="application-name" content="Danger Admin" />
                    <meta name="msapplication-TileColor" content="#ffffff" />
                    <meta name="theme-color" content="#ffffff" />

                    {this.props.styleTags}
                    {this.helmetHeadComponents}
                </Head>
                <body {...this.helmetBodyAttrComponents}>
                    <Main />
                    <NextScript />
                </body>
            </html>
        );
    }
}
