import { inject, observer } from "mobx-react";
import { withRouter } from "next/router";
import PT from "prop-types";
import React, { Component } from "react";
import {
    AdminForm,
    AdminTabs,
    AdminTextInput,
    AdminTextarea,
    AdminTitleInput,
    AdminTranslateTab,
    AdminDatePicker,
} from "redline-admin";
import * as Yup from "yup";
import { Layout, CustomAdminContent, MetaTabFields } from "../components";
import { languages } from "../config/langguages";
import routes from "../routes";
import { translateSchemaRule, securePageHoc } from "../utils";

const schema = Yup.object().shape({
    published_at: Yup.date().required("Выбрать дату публикации"),
    ...translateSchemaRule("title", Yup.string().required("Введите заголовок")),
    slug: Yup.string().required("Введите slug"),
    //
    ...translateSchemaRule(
        "alt_title",
        Yup.string().required("Введите альтернативный заголовок")
    ),
    //
    ...translateSchemaRule(
        "description",
        Yup.string().required("Введите описание категории")
    ),
});

@securePageHoc
@withRouter
@inject("category_store")
@observer
class CategoryDetailPage extends Component {
    static propTypes = {
        category_store: PT.object.isRequired,
        router: PT.object.isRequired,
    };

    componentDidMount() {
        const { category_store, router } = this.props;
        if (router.query.id !== "_") {
            category_store.getOne(router.query.id);
        } else {
            category_store.reset();
            category_store.setReady();
        }
    }

    render() {
        const {
            category_store: { item },
            category_store,
        } = this.props;
        return (
            <Layout>
                <CustomAdminContent
                    routes={routes}
                    title={
                        (() => false)()
                            ? "Добавление категории"
                            : "Редактирование категории"
                    }
                >
                    <AdminForm
                        item={item}
                        store={category_store}
                        router={routes.Router}
                        validationSchema={schema}
                    >
                        <AdminTabs>
                            <AdminTabs.TabPane tab="Основное" key="1">
                                <AdminDatePicker
                                    name="published_at"
                                    placeholder="Выбрать дату"
                                    label="Дата публикации"
                                />
                                <AdminTranslateTab
                                    name="title"
                                    languages={languages}
                                >
                                    <AdminTitleInput
                                        autoslug={!item || !item.id}
                                        label="Заголовок"
                                        slug_name="slug"
                                        slug_label="Слаг"
                                    />
                                </AdminTranslateTab>
                                <AdminTranslateTab
                                    name="alt_title"
                                    languages={languages}
                                >
                                    <AdminTextInput label="Альтернативный заголовок" />
                                </AdminTranslateTab>
                                <AdminTranslateTab
                                    name="description"
                                    languages={languages}
                                >
                                    <AdminTextarea label="Описание" />
                                </AdminTranslateTab>
                            </AdminTabs.TabPane>
                            <AdminTabs.TabPane tab="SEO" key="2">
                                <MetaTabFields />
                            </AdminTabs.TabPane>
                        </AdminTabs>
                    </AdminForm>
                </CustomAdminContent>
            </Layout>
        );
    }
}

export default CategoryDetailPage;
