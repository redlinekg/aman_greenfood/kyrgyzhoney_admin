import React, { Component } from "react";
import { styled } from "linaria/react";
import { position } from "polished";

import { AdminSettings, antd } from "redline-admin";
import { CustomAdminContent, Layout } from "../components";
import routes from "../routes";
import { settingsApi, securePageHoc } from "../utils";

const { Button } = antd;

@securePageHoc
class GlobalDetailPage extends Component {
    state = {
        settings: [],
        editable: false,
    };

    componentDidMount() {
        this.getSettings();
    }

    getSettings = async () => {
        const settings = await settingsApi.getSections();
        this.setState({
            settings: settings,
        });
    };

    sendSettings = async () => {
        const settings = await settingsApi.sendSettings(this.state.settings);
        this.setState({
            settings: settings,
        });
    };

    onChange = (new_settigns) => {
        this.setState({
            settings: new_settigns,
        });
    };

    editableToggle = () => {
        this.setState({
            editable: !this.state.editable,
        });
    };

    render() {
        const { editable } = this.state;
        return (
            <Layout>
                <CustomAdminContent
                    routes={routes}
                    title="Настройки"
                    fit
                    hide_bg
                    back_button_show={false}
                >
                    <SBlock>
                        <SEditableToggle onClick={this.editableToggle} />
                        <AdminSettings
                            settings={this.state.settings}
                            onChange={this.onChange}
                            editable={editable}
                        />
                        <br />
                        <Button onClick={this.sendSettings} type="primary">
                            Сохранить
                        </Button>
                    </SBlock>
                </CustomAdminContent>
            </Layout>
        );
    }
}

const SBlock = styled.div`
    position: relative;
`;
const SEditableToggle = styled.div`
    ${position("fixed", "0", null, null, "0")};
    width: 10px;
    height: 10px;
    z-index: 100;
`;

export default GlobalDetailPage;
