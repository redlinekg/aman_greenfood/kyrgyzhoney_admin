import { inject, observer } from "mobx-react";
import PT from "prop-types";
import React, { Component } from "react";
import {
    AdminTable,
    AdminTableImage,
    AdminTableOperation,
    AdminTableTitle,
} from "redline-admin";
import { Layout, CustomAdminContent } from "../components";
import routes from "../routes";
import { securePageHoc } from "../utils";

const COLUMNS = [
    {
        title: "Аватар",
        dataIndex: "avatar",
        key: "avatar",
        render: (avatar) => (
            <AdminTableImage src={avatar ? avatar.url_path : null} />
        ),
        width: "1%",
    },
    {
        title: "Заголовок",
        dataIndex: "title",
        key: "title",
        render: (text, item) => (
            <AdminTableTitle
                title={text}
                route="review_detail"
                params={{ id: item.id + "" }}
            />
        ),
        width: "50%",
    },
    {
        title: "Действие",
        dataIndex: "actions",
        key: "actions",
        width: "1%",
        render: (_, item) => (
            <AdminTableOperation
                item={item}
                route="review_detail"
                params={{ id: item.id + "" }}
                remove={false}
            />
        ),
    },
];

@securePageHoc
@inject("reviews_store")
@observer
class ReviewList extends Component {
    static propTypes = {
        reviews_store: PT.object,
    };

    componentDidMount() {
        this.props.reviews_store.getList();
    }

    render() {
        const {
            reviews_store: { objects },
        } = this.props;
        return (
            <Layout>
                <CustomAdminContent
                    routes={routes}
                    title="Отзывы"
                    desk={`${objects.length} отзывов`}
                    create_route="review_detail"
                    fit
                    back_button_show={false}
                >
                    <AdminTable columns={COLUMNS} dataSource={objects} />
                </CustomAdminContent>
            </Layout>
        );
    }
}

export default ReviewList;
