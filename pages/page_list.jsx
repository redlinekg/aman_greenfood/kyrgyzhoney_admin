import {
    AdminTable,
    AdminTableOperation,
    AdminTableTitle,
} from "redline-admin";
import React, { Component } from "react";
import { inject, observer } from "mobx-react";

import { Layout, CustomAdminContent } from "../components";
import PT from "prop-types";
import routes from "../routes";
import { securePageHoc } from "../utils";

const COLUMNS = [
    {
        title: "Заголовок",
        dataIndex: "title",
        key: "title",
        render: (text, item) => (
            <AdminTableTitle
                title={text}
                route="page_detail"
                params={{ id: item.id + "" }}
            />
        ),
        width: "50%",
    },
    {
        title: "Действие",
        dataIndex: "actions",
        key: "actions",
        width: "1%",
        render: (_, item) => (
            <AdminTableOperation
                item={item}
                route="page_detail"
                params={{ id: item.id + "" }}
                remove={false}
            />
        ),
    },
];

@securePageHoc
@inject("pages_store")
@observer
class PageList extends Component {
    static propTypes = {
        pages_store: PT.object,
    };

    componentDidMount() {
        this.props.pages_store.getList();
    }

    render() {
        const {
            pages_store: { objects },
        } = this.props;
        return (
            <Layout>
                <CustomAdminContent
                    routes={routes}
                    title="Страницы"
                    desk={`${objects.length} страницы`}
                    create_route="page_detail"
                    fit
                    back_button_show={false}
                >
                    <AdminTable columns={COLUMNS} dataSource={objects} />
                </CustomAdminContent>
            </Layout>
        );
    }
}

export default PageList;
