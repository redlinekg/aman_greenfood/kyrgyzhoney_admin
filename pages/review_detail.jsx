import React, { Component } from "react";
import PT from "prop-types";
import { inject, observer } from "mobx-react";
import { Divider, Typography } from "antd";
import * as Yup from "yup";
import { withRouter } from "next/router";

import {
    Layout, 
    CustomAdminEditorJs as AdminEditorJs,
    CustomAdminContent,
    CustomAdminUploader,
} from "../components";
import {
    AdminForm,
    AdminSelect,
    AdminTextInput,
    AdminTranslateTab,
} from "redline-admin";
import { languages } from "../config/langguages";
import routes from "../routes";
import { translateSchemaRule, securePageHoc } from "../utils";

const schema = Yup.object().shape({
    avatar: Yup.object(),
    //
    ...translateSchemaRule("name", Yup.string().required("Введите имя")),
    //
    ...translateSchemaRule("country", Yup.string().required("Введите страну")),
    //
    social_type: Yup.string().required("Выберите тип ссылки"),
    social_link: Yup.string().required("Введите ссылку"),
    //
    ...translateSchemaRule(
        "title",
        Yup.string().required("Введите заголовок отзывы")
    ),
    //
    ...translateSchemaRule(
        "body",
        Yup.string().required("Введите текст отзыва")
    ),
});

@securePageHoc
@withRouter
@inject("review_store")
@observer
class ReviewDetail extends Component {
    static propTypes = {
        review_store: PT.object.isRequired,
        router: PT.object.isRequired,
    };

    componentDidMount() {
        const { review_store, router } = this.props;
        if (router.query.id !== "_") {
            review_store.getOne(router.query.id);
        } else {
            review_store.reset();
            review_store.setReady();
        }
    }

    render() {
        const {
            review_store: { item },
            review_store,
        } = this.props;
        return (
            <Layout>
                <CustomAdminContent
                    routes={routes}
                    title={
                        (() => false)()
                            ? "Добавление отзыва"
                            : "Редактирование отзыва"
                    }
                >
                    <AdminForm
                        item={item}
                        store={review_store}
                        router={routes.Router}
                        validationSchema={schema}
                    >
                        <CustomAdminUploader
                            name="avatar"
                            label="Фото для аватара"
                        />
                        <AdminTranslateTab name="name" languages={languages}>
                            <AdminTextInput label="Имя" />
                        </AdminTranslateTab>
                        <AdminTranslateTab name="country" languages={languages}>
                            <AdminTextInput label="Страна" />
                        </AdminTranslateTab>
                        <Divider />
                        <Typography.Title level={4}>
                            Ссылка на соц сеть
                        </Typography.Title>
                        <AdminSelect
                            label="Тип ссылки"
                            placeholder="Выберите тип"
                            name="social_type"
                            variants={[
                                { value: "telegram", label: "telegram" },
                                { value: "whatsapp", label: "whatsapp" },
                                { value: "skype", label: "skype" },
                                { value: "facebook", label: "facebook" },
                                { value: "instagram", label: "instagram" },
                                { value: "twitter", label: "twitter" },
                                { value: "link", label: "Ссылка" },
                            ]}
                        />
                        <AdminTextInput name="social_link" label="Ссылка" />
                        <Divider />
                        <AdminTranslateTab name="title" languages={languages}>
                            <AdminTextInput label="Тема отзыва" />
                        </AdminTranslateTab>
                        <AdminTranslateTab name="body" languages={languages}>
                            <AdminEditorJs label="Текст отзыва" />
                        </AdminTranslateTab>
                    </AdminForm>
                </CustomAdminContent>
            </Layout>
        );
    }
}

export default ReviewDetail;
