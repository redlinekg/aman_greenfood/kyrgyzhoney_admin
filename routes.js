const nextRoutes = require("@vlzh/next-routes");
const routes = (module.exports = nextRoutes.default());

routes.add({
    name: "index",
    pattern: "/",
    page: "index"
});

/////////////////////////////////////////////////////////////
//Обычные страницы
routes.add({
    name: "page_list",
    pattern: "/page_list",
    page: "page_list"
});
routes.add({
    name: "page_detail",
    pattern: "/page_detail/:id",
    page: "page_detail"
});

/////////////////////////////////////////////////////////////

// Категории товаров
routes.add({
    name: "category_list",
    pattern: "/category_list",
    page: "category_list"
});
routes.add({
    name: "category_detail",
    pattern: "/category_detail/:id",
    page: "category_detail"
});

/////////////////////////////////////////////////////////////

// Товары
routes.add({
    name: "product_list",
    pattern: "/product_list",
    page: "product_list"
});
routes.add({
    name: "product_detail",
    pattern: "/product_detail/:id",
    page: "product_detail"
});

/////////////////////////////////////////////////////////////

// Контакты
routes.add({
    name: "contact_list",
    pattern: "/contact_list",
    page: "contact_list"
});
routes.add({
    name: "contact_detail",
    pattern: "/contact_detail/:id",
    page: "contact_detail"
});

/////////////////////////////////////////////////////////////

//Блог
routes.add({
    name: "article_list",
    pattern: "/article_list",
    page: "article_list"
});
routes.add({
    name: "article_detail",
    pattern: "/article_detail/:id",
    page: "article_detail"
});

// Авторы блога
routes.add({
    name: "article_author_list",
    pattern: "/article_author_list",
    page: "article_author_list"
});
routes.add({
    name: "article_author_detail",
    pattern: "/article_author_detail/:id",
    page: "article_author_detail"
});

/////////////////////////////////////////////////////////////

// Отзывы
routes.add({
    name: "review_list",
    pattern: "/review_list",
    page: "review_list"
});
routes.add({
    name: "review_detail",
    pattern: "/review_detail/:id",
    page: "review_detail"
});

/////////////////////////////////////////////////////////////

// Слайды
routes.add({
    name: "slide_list",
    pattern: "/slide_list",
    page: "slide_list"
});
routes.add({
    name: "slide_detail",
    pattern: "/slide_detail/:id",
    page: "slide_detail"
});

/////////////////////////////////////////////////////////////

//Общие данные
routes.add({
    name: "global_detail",
    pattern: "/global_detail",
    page: "global_detail"
});

/////////////////////////////////////////////////////////////

//Авторизация
routes.add({
    name: "login",
    pattern: "/login",
    page: "login"
});
routes.add({
    name: "user_list",
    pattern: "/user_list",
    page: "user_list"
});
routes.add({
    name: "user_detail",
    pattern: "/user_detail/:id",
    page: "user_detail"
});
