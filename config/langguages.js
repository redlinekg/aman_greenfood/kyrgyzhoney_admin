export const languages = [
    {
        name: "Russian",
        code: "ru"
    },
    {
        name: "English",
        code: "en"
    },
    {
        name: "Kyrgyz",
        code: "kg"
    },
    {
        name: "Chinese",
        code: "cn"
    }
];
