export const UPLOAD_IMAGE_ENDPOINT = "/api/uploads/image";
export const UPLOAD_FILE_ENDPOINT = "/api/uploads/file";
export const UPLOAD_FILE_EDITORJS_ENDPOINT = "/api/uploads/file_editorjs";
export const FETCH_URL_ENDPOINT = "/fetchUrl";
export const SETTINGS_ENDPOINT = "/api/settings";
export const SITE_URL = "/";
