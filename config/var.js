import { lighten } from "polished";

export const grey_list = {
    grey: "#5E5E5E",
};

export const colors = {
    //global
    body: "#ffffff",
    // colors
    white: "#fff",
    black: "#292929",

    blue: "#006CA1",
    red: "#ed253f",
    violet: "#586CB3",
    yellow: "#FCC32E",

    green: "#26A69B",
    // grey colors
    grey: grey_list.grey,
    grey_bor: lighten(0.57, grey_list.grey),
    grey_light: lighten(0.3, grey_list.grey),
    grey_light2: lighten(0.4, grey_list.grey),
    grey_light3: lighten(0.5, grey_list.grey),
    grey_light4: lighten(0.55, grey_list.grey),
    grey_light5: lighten(0.58, grey_list.grey),
};

export const global = {
    padding_content: "20px",
    sidebar_width: "265px",
    border_radius: "0.25rem",
};

export const fonts = {
    global_font: "'Harmonia Sans Pro Cyr', sans-serif",
};

export const grid = {
    tn: 372,
    sm: 576,
    md: 768,
    lg: 992,
    xl: 1200,
    xxl: 1480,
};

export const header_num = {
    height: 120,
    height_top: 60,
    height_bottom: 50,
};

export const header = {
    height: `${header_num.height_top + header_num.height_bottom}px`,
    height_top: `${header_num.height_top}px`,
    height_bottom: `${header_num.height_bottom}px`,
};

export const url = "http://stompharm.kg";
