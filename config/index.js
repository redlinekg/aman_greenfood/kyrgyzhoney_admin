import { languages } from "./langguages";
import * as urls from "./urls";
import * as vars from "./var";

export { urls, languages, vars };
