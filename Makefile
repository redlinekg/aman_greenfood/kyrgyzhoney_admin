TAG=registry.gitlab.com/redlinekg/aman_greenfood/kyrgyzhoney_admin

.PHONY: all build_admin publish_admin

all: build_admin publish_admin

build_admin:
	telegram-send '[kyrgyzhoney build_admin] start'
	cp -RfL ./node_modules/kyrgyzhoney_mobx ./
	rm -Rf ./kyrgyzhoney_mobx/node_modules
	docker build -t $(TAG) .
	rm -Rf ./kyrgyzhoney_mobx
	telegram-send '[kyrgyzhoney build_admin] end'

publish_admin:
	telegram-send '[kyrgyzhoney publish_admin] start'
	docker push $(TAG)
	telegram-send '[kyrgyzhoney publish_admin] end'
