const next = require("next");
const express = require("express");
const { createProxyMiddleware } = require("http-proxy-middleware");
const routes = require("./routes");

// envs
const PORT = parseInt(process.env.PORT, 10) || 3001;
const HOST = parseInt(process.env.HOST, 10) || "0.0.0.0";
const DEV = process.env.NODE_ENV !== "production";
const API_URL = process.env.API_URL;
const MEDIA_REDIRECT_URL = process.env.MEDIA_REDIRECT_URL;

//
const app = next({ dev: DEV });
const handler = routes.getRequestHandler(app);
//
app.prepare().then(() => {
    const server = express();

    if (DEV) {
        server.use(
            createProxyMiddleware("/uploads", {
                target: MEDIA_REDIRECT_URL,
                changeOrigin: true,
                xfwd: true,
            })
        );
        server.use(
            createProxyMiddleware("/fetchUrl", {
                target: API_URL,
                changeOrigin: true,
                xfwd: true,
            })
        );
        server.use(
            createProxyMiddleware("/api", {
                target: API_URL,
                changeOrigin: true,
                xfwd: true,
                followRedirects: true,
            })
        );
    }

    server.all("*", (req, res) => handler(req, res));

    server.listen(PORT, HOST, (err) => {
        if (err) {
            throw err;
        }
        console.log(`> Ready on ${HOST}:${PORT}`);
    });
});
