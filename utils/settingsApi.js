import axios from "axios";
import { SETTINGS_ENDPOINT } from "../config/urls";

class SettingsApi {
    async getSections() {
        const response = await axios.get(SETTINGS_ENDPOINT);
        return response.data;
    }
    async sendSettings(value) {
        const response = await axios.post(SETTINGS_ENDPOINT, value);
        return response.data;
    }
}

export default new SettingsApi();
