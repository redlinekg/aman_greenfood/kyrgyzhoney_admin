import * as Yup from "yup";
import { translateSchemaRule } from "../utils";

const getMetaSchema = () => ({
    ...translateSchemaRule("meta_title", Yup.string().nullable()),
    meta_image: Yup.object().nullable(),
    ...translateSchemaRule("meta_description", Yup.string().nullable())
});

export default getMetaSchema;
