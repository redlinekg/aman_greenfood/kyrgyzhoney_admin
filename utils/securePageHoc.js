import React from "react";
import PropTypes from "prop-types";
import Login from "../pages/login";
import { Router } from "../routes";

const securePageHoc = (Page) =>
    class SecurePage extends React.Component {
        static propTypes = {
            isAuthenticated: PropTypes.bool,
        };

        static getInitialProps(ctx) {
            return Page.getInitialProps && Page.getInitialProps(ctx);
        }

        componentDidMount() {
            if (!this.props.isAuthenticated) {
                Router.replace("/login");
            }
        }

        render() {
            const { isAuthenticated } = this.props;
            return isAuthenticated ? <Page {...this.props} /> : <Login />;
        }
    };

export default securePageHoc;
