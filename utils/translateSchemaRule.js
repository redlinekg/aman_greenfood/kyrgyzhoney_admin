export const translateSchemaRule = (name, rule) => {
    return {
        [name]: rule,
        [name + "__kg"]: rule,
        [name + "__en"]: rule,
        [name + "__cn"]: rule
    };
};
