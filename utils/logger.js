import pino from "pino";

const logger = pino({
    level: "debug",
    useLevelLabels: true,
    browser: {
        asObject: true,
        write: (o) => {
            console.log(o);
        },
    },
    prettyPrint: true,
});

export default logger;
