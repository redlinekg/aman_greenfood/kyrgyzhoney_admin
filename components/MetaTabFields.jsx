import {
    AdminTextInput,
    AdminTextarea,
    AdminTranslateTab
} from "redline-admin";

import CustomAdminUploader from "./CustomAdminUploader";
import React from "react";
import { languages } from "../config/langguages";

const MetaTabFields = () => (
    <>
        <AdminTranslateTab name="meta_title" languages={languages}>
            <AdminTextInput label="Мета заголовок" />
        </AdminTranslateTab>
        <CustomAdminUploader name="meta_image" label="Мета изображение" />
        <AdminTranslateTab name="meta_description" languages={languages}>
            <AdminTextarea label="Мета описание" />
        </AdminTranslateTab>
    </>
);

export default MetaTabFields;
