import React, { Component } from "react";
import { Helmet } from "react-helmet";
import Head from "next/head";
import PT from "prop-types";

const concatMeta = (...meta_objects) =>
    meta_objects.reduce((result, current) => {
        if (current) {
            result.push(current);
        }
        return result;
    }, []);

class Meta extends Component {
    static defaultProps = {
        part: " - Danger Admin",
    };

    static propTypes = {
        title: PT.string.isRequired,
        part: PT.string,
    };

    render() {
        const { title, part } = this.props;
        return (
            <>
                <Head>
                    <title>{`${title ? title : "Загрузка"} ${part}`}</title>
                </Head>
                <Helmet
                    meta={concatMeta(
                        {
                            property: "og:type",
                            content: "website",
                        },
                        {
                            property: "og:site_name",
                            content: "ADMIN",
                        }
                    )}
                />
            </>
        );
    }
}

export default Meta;
