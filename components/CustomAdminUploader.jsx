import React from "react";
import { AdminUploader } from "redline-admin";
import { UPLOAD_IMAGE_ENDPOINT } from "../config/urls";

const urlExecutor = response => response.url_path;

const CustomAdminUploader = props => {
    return (
        <AdminUploader
            upload_url={UPLOAD_IMAGE_ENDPOINT}
            url_executor={urlExecutor}
            size_limit={20 * 1024 * 1024}
            {...props}
        />
    );
};

export default CustomAdminUploader;
