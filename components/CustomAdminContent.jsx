import { AdminContent, AdminHeader } from "redline-admin";
import React, { Component } from "react";
import { inject, observer } from "mobx-react";

import PT from "prop-types";
import { Router } from "../routes";

class CustomAdminHeader extends Component {
    static propTypes = {
        auth_store: PT.object,
    };
    render() {
        const { auth_store, ...rest } = this.props;
        return (
            <AdminHeader
                {...rest}
                header_username={auth_store.user?.email}
                onLogout={() => {
                    auth_store.logout();
                    Router.push("/");
                }}
            />
        );
    }
}

@inject("auth_store")
@observer
class CustomAdminContent extends Component {
    render() {
        const {
            desk,
            fit,
            hide_bg,
            auth_store,
            back_button_show,
            ...props
        } = this.props;
        auth_store.user;
        return (
            <AdminContent
                header_show={false}
                description={desk}
                show_content_wrapper={fit}
                show_bg={hide_bg}
                show_tools_line
                back_button_show={back_button_show}
                header={
                    <CustomAdminHeader
                        header_link="http://kyrgyzstanhoney.com"
                        header_link_text="kyrgyzstanhoney.com"
                        auth_store={auth_store}
                    />
                }
                {...props}
            />
        );
    }
}

CustomAdminContent.defaultProps = {
    back_button_show: true,
};

CustomAdminContent.propTypes = {
    desk: PT.string,
    fit: PT.bool,
    hide_bg: PT.bool,
    onLogout: PT.string,
    auth_store: PT.object,
    back_button_show: PT.bool,
};

export default CustomAdminContent;
