import React from "react";
import { AdminMultiUploader } from "redline-admin";
import { UPLOAD_IMAGE_ENDPOINT } from "../config/urls";

const CustomAdminMultiUploader = props => {
    return (
        <AdminMultiUploader
            upload_url={UPLOAD_IMAGE_ENDPOINT}
            convertValue={v => ({
                uid: v.id + "",
                name: `name${v.id}`,
                status: "done",
                url: v.url_path
            })}
            {...props}
        />
    );
};

export default CustomAdminMultiUploader;
