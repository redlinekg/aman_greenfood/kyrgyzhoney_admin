import React from "react";
import PT from "prop-types";
import { styled } from "linaria/react";
import dynamic from "next/dynamic";
const AdminSidebar = dynamic(
    () => import("redline-admin").then((module) => module.AdminSidebar),
    {
        ssr: false,
    }
);
import { AdminLayoutBlock } from "redline-admin";

import routes from "../routes";

const sidebar_menu = [
    {
        route: "product_list",
        title: "Товары",
        icon: "ProductIcon",
        height: 0,
    },
    {
        route: "article_list",
        title: "Записи блога",
        icon: "ArticleIcon",
        height: 0,
    },
    {
        route: "slide_list",
        title: "Слайды на главной",
        icon: "SlideIcon",
        height: 0,
    },
    {
        route: "page_list",
        title: "Страницы",
        icon: "PageIcon",
        height: 0,
        delimiter: true,
    },
    {
        route: "category_list",
        title: "Категории товаров",
        icon: "CategoryIcon",
        height: 0,
    },
    {
        route: "article_author_list",
        title: "Авторы блога",
        icon: "AuthorIcon",
        height: 0,
    },
    {
        route: "review_list",
        title: "Отзывы",
        icon: "RewiewIcon",
        height: 0,
    },
    {
        route: "contact_list",
        title: "Контакты",
        icon: "ContactIcon",
        height: 0,
    },
    {
        route: "global_detail",
        title: "Настройки",
        icon: "SettingIcon",
        height: 0,
    },
];

const Layout = ({ children }) => {
    return (
        <SLayout>
            <AdminSidebar menu_config={sidebar_menu} routes={routes} />
            <AdminLayoutBlock>{children}</AdminLayoutBlock>
        </SLayout>
    );
};

Layout.propTypes = {
    children: PT.any.isRequired,
};

const SLayout = styled.div``;

export default Layout;
