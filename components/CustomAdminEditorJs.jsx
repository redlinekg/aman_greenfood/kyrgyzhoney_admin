import {
    FETCH_URL_ENDPOINT,
    UPLOAD_FILE_EDITORJS_ENDPOINT,
    UPLOAD_IMAGE_ENDPOINT
} from "../config/urls";

import { AdminEditorJs } from "redline-admin";
import PT from "prop-types";
import React from "react";

const imageUrlExecutor = response => {
    return response.url_path;
};

const CustomAdminEditorJs = ({ translate_lang, ...props }) => {
    const holder = translate_lang ? `editor-js-${translate_lang}` : "editor-js";

    return (
        <AdminEditorJs
            image_upload_url={UPLOAD_IMAGE_ENDPOINT}
            image_url_executor={imageUrlExecutor}
            file_upload_url={UPLOAD_FILE_EDITORJS_ENDPOINT}
            link_endpoint={FETCH_URL_ENDPOINT}
            holder={holder}
            {...props}
        >
            <div id={holder}></div>
        </AdminEditorJs>
    );
};
CustomAdminEditorJs.propTypes = {
    translate_lang: PT.string
};

export default CustomAdminEditorJs;
