// next.config.js
const withPlugins = require("next-compose-plugins");
const withTM = require("next-transpile-modules");
const withImages = require("next-images");
const withCss = require("@zeit/next-css");

const IS_DEV = process.env.NODE_ENV !== "production";

module.exports = withPlugins(
    [[withTM(["stompharm-mobx", "redline-admin"])], withCss, withImages],
    {
        webpack: (config) => {
            config.devtool = IS_DEV ? "eval" : "cheap-source-map";
            config.resolve.symlinks = false;

            let nextBabelLoader = config.module.rules[0].use;
            config.module.rules[0].use = [
                ...(Array.isArray(nextBabelLoader)
                    ? nextBabelLoader
                    : [nextBabelLoader]),
                {
                    loader: "linaria/loader",
                    options: {
                        sourceMap: process.env.NODE_ENV !== "production",
                    },
                },
            ];
            return config;
        },
        env: {
            API_URL: process.env.API_URL,
        },
    }
);
